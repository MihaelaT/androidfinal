package com.example.mihaela.proiectmihaela;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class AdapterBazaDate {

    SQLiteOpenHelper helper;
    SQLiteDatabase db;

    public AdapterBazaDate(Context ctx, int versiune){
        helper = new HelperBD(ctx, DenumiriBD.NUME_BD,null, versiune);
    }

    public void deschideConexiuneBD(){
        db=helper.getWritableDatabase();
    }

    public void inchideConexiuneBD(){
        db.close();
    }

    //inserare in tabela Utilizator
    public void inserareUtilizatori(Utilizator u){
        ContentValues contentV =new ContentValues();
        contentV.put(DenumiriBD.NUME_PRENUME_COLUMN, u.getNumePrenume());
        contentV.put(DenumiriBD.EMAIL_COLUMN,u.getEmail());
        contentV.put(DenumiriBD.USERNAME_COLUMN,u.getUserName());
        contentV.put(DenumiriBD.PAROLA_COLUMN,u.getParola());
        contentV.put(DenumiriBD.FACULTATE_COLUMN,u.getFacultate());
        contentV.put(DenumiriBD.TIP_CONT_COLUMN,u.getTipCont());
        contentV.put(DenumiriBD.ID_SITUATIE_U_COLUMN,u.getIdSituatie());
        db.insert(DenumiriBD.TABELA_UTILIZATOR,null,contentV);
    }

    //inserare in tabela Test
    public void inserareTest(Test t){
        ContentValues contentV =new ContentValues();
        contentV.put(DenumiriBD.ID_TEST_COLUMN, t.getIdTest());
        contentV.put(DenumiriBD.NR_INTREBARI_COLUMN,t.getNrIntrebari());
      //  contentV.put(DenumiriBD.ID_INTREBARE_T_COLUMN,t.getIdIntrebare());
        contentV.put(DenumiriBD.NOTA_COLUMN,t.getNota());
        db.insert(DenumiriBD.TABELA_TEST,null,contentV);
    }

    //inserare in tabela Intrebare
    public void inserareIntrebare(Intrebare intrebare){
        ContentValues contentV =new ContentValues();
        contentV.put(DenumiriBD.ID_INTREBARE_COLUMN, intrebare.getIdIntrebare());
        contentV.put(DenumiriBD.INTREBARE_COLUMN,intrebare.getIntrebare());
        contentV.put(DenumiriBD.RASPUNS1_COLUMN,intrebare.getRaspuns1());
        contentV.put(DenumiriBD.RASPUNS2_COLUMN,intrebare.getRaspuns2());
        contentV.put(DenumiriBD.RASPUNS3_COLUMN,intrebare.getRaspuns3());
        contentV.put(DenumiriBD.RASPUNS4_COLUMN,intrebare.getRaspuns4());
        contentV.put(DenumiriBD.CORECT_COLUMN,intrebare.getIdCorect());
        db.insert(DenumiriBD.TABELA_INTREBARE,null,contentV);
    }

    //inserare in tabela Situatie
    public void inserareSituatie(SituatieUtilizator situatieUt){
        ContentValues contentV =new ContentValues();
        contentV.put(DenumiriBD.ID_SITUATIE_S_COLUMN, situatieUt.getIdSituatie());
        contentV.put(DenumiriBD.ID_TEST_COLUMN,situatieUt.getIdTest());
        contentV.put(DenumiriBD.SCOR_TOTAL_COLUMN,situatieUt.getScorTotalU());
        db.insert(DenumiriBD.TABELA_SITUATIE_U,null,contentV);
    }

    //inserare in tabela Studenti
    public void inserareStudent(Student student){
        ContentValues contentV =new ContentValues();
        contentV.put(DenumiriBD.USERNAME_STUD_COLUMN, student.getUsername());
        contentV.put(DenumiriBD.CNP_COLUMN,student.getCnp());
        contentV.put(DenumiriBD.GRUPA_COLUMN,student.getGrupa());
        contentV.put(DenumiriBD.SERIA_COLUMN, student.getSeria());
        db.insert(DenumiriBD.TABELA_Studenti,null,contentV);
    }

    //select user dupa datele introduse la login
    public Utilizator getUserByLoginData (String username, String parola)
    {
        Utilizator u;

        Cursor c = db.query(DenumiriBD.TABELA_UTILIZATOR,
                new String[] {DenumiriBD.NUME_PRENUME_COLUMN, DenumiriBD.EMAIL_COLUMN, DenumiriBD.USERNAME_COLUMN,
                DenumiriBD.PAROLA_COLUMN, DenumiriBD.FACULTATE_COLUMN, DenumiriBD.TIP_CONT_COLUMN},
                DenumiriBD.USERNAME_COLUMN+" = '"+ username+"'"+" AND "+ DenumiriBD.PAROLA_COLUMN + " = '"+ parola+"' ",
                null,null,null,null);

        if(c!=null && c.getCount() == 1){
            c.moveToFirst();
            u = new Utilizator(c.getString(0),c.getString(1),c.getString(2),
                    c.getString(3),c.getString(4),c.getString(5),0);
        }
        else {
            u = null;
        }
         c.close();
        return u;
    }

    //returneaza datele unui utilizator dupa username
    public Utilizator getUserByUsername (String username)
    {
        Utilizator u;

        Cursor c = db.query(DenumiriBD.TABELA_UTILIZATOR,
                new String[] {DenumiriBD.NUME_PRENUME_COLUMN, DenumiriBD.EMAIL_COLUMN, DenumiriBD.USERNAME_COLUMN,
                        DenumiriBD.PAROLA_COLUMN, DenumiriBD.FACULTATE_COLUMN, DenumiriBD.TIP_CONT_COLUMN},
                DenumiriBD.USERNAME_COLUMN+" = '"+ username+"'",
                null,null,null,null);

        if(c!=null && c.getCount() == 1){
            c.moveToFirst();
            u = new Utilizator(c.getString(0),c.getString(1),c.getString(2),
                    c.getString(3),c.getString(4),c.getString(5),0);
        }
        else {
            u = null;
        }

        c.close();
        return u;
    }

    //verificare daca un user exista in BD
    public boolean existaUser (String username)
    {
        Cursor c = db.query(DenumiriBD.TABELA_UTILIZATOR, new String[] {DenumiriBD.NUME_PRENUME_COLUMN},
                DenumiriBD.USERNAME_COLUMN+" = '"+ username+"'",
                null,null,null,null);

        if(c!=null && c.getCount()!=0) {
            c.close();
            return true; //exista
        }
        else {
            c.close();
            return false; //nu exista}
        }
    }

    //select student dupa username
    public Student getDateStudent (String username)
    {
         Student stud;

        Cursor cursor = db.query(DenumiriBD.TABELA_Studenti,
                new String[] {DenumiriBD.USERNAME_STUD_COLUMN, DenumiriBD.CNP_COLUMN, DenumiriBD.GRUPA_COLUMN,DenumiriBD.SERIA_COLUMN},
                DenumiriBD.USERNAME_STUD_COLUMN+ " = '"+username+"'",null,null,null,null);

        if(cursor!=null && cursor.getCount() == 1){
            cursor.moveToFirst();
            stud = new Student(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3));
        }
        else {
            stud = null;
        }

        cursor.close();
        return stud;
    }

    //update in tabela Utilizator
    public void updateUtilizatori(Utilizator u){
        ContentValues contentV =new ContentValues();
        contentV.put(DenumiriBD.NUME_PRENUME_COLUMN, u.getNumePrenume());
        contentV.put(DenumiriBD.EMAIL_COLUMN,u.getEmail());
        contentV.put(DenumiriBD.FACULTATE_COLUMN,u.getFacultate());

        db.update(DenumiriBD.TABELA_UTILIZATOR,contentV,
                DenumiriBD.USERNAME_COLUMN +" = ? ",new String[]{u.getUserName()});
    }
    //update in tabela Studenti
    public void updateStudenti(Student student){
        ContentValues contentV = new ContentValues();
        contentV.put(DenumiriBD.CNP_COLUMN,student.getCnp());
        contentV.put(DenumiriBD.GRUPA_COLUMN,student.getGrupa());
        contentV.put(DenumiriBD.SERIA_COLUMN, student.getSeria());

        db.update(DenumiriBD.TABELA_Studenti,contentV,
                DenumiriBD.USERNAME_COLUMN +" = ? ",new String[]{student.getUsername()});
    }

    //delete User by username
    public void deleteUtilizatori(String username) {
        db.delete(DenumiriBD.TABELA_UTILIZATOR, DenumiriBD.USERNAME_COLUMN + " = ? ", new String[]{username});
    }
    //delete Student by username
    public void deleteStudent(String username) {
        db.delete(DenumiriBD.TABELA_Studenti, DenumiriBD.USERNAME_COLUMN + " = ? ", new String[]{username});
    }

    //selectam numarul total de intrebari din BD
    public int selectCountIntrebare(){
        int nrIntrebari = 0;

        Cursor cursor = db.query(DenumiriBD.TABELA_INTREBARE,
                new String[] {DenumiriBD.ID_INTREBARE_COLUMN},
                null,null,null,null,null);

      nrIntrebari = cursor.getCount();
      cursor.close();
        return nrIntrebari;
    }
    //selectam toate intrebarile din BD
    public ArrayList<Intrebare> selectAllIntrebare(){
        ArrayList<Intrebare> intrebari = new ArrayList<>();

        Cursor cursor = db.query(DenumiriBD.TABELA_INTREBARE,
                new String[] {DenumiriBD.ID_INTREBARE_COLUMN, DenumiriBD.INTREBARE_COLUMN,DenumiriBD.RASPUNS1_COLUMN,
                        DenumiriBD.RASPUNS2_COLUMN, DenumiriBD.RASPUNS3_COLUMN, DenumiriBD.RASPUNS4_COLUMN, DenumiriBD.CORECT_COLUMN},
                null,null,null,null,null);

         if(cursor!=null && cursor.getCount() > 0)
         {
             cursor.moveToFirst();
             do {
                 intrebari.add(new Intrebare(cursor.getInt(0),cursor.getString(1),cursor.getString(2),
                         cursor.getString(3),cursor.getString(4),
                         cursor.getString(5),cursor.getInt(6)));
             }while (cursor.moveToNext());
         }
        cursor.close();
        return intrebari;
    }

    //delete Intrebare by intrebare text
    public void deleteIntrebare(String intrebareText) {
        db.delete(DenumiriBD.TABELA_INTREBARE, DenumiriBD.INTREBARE_COLUMN + " = ? ", new String[]{intrebareText});
    }

    public int countIntrebariCateg (int codCategorie){
        int nrIntrebari = 0;

        Cursor cursor = db.query(DenumiriBD.TABELA_INTREBARE,
                new String[] {DenumiriBD.ID_INTREBARE_COLUMN},
                DenumiriBD.ID_INTREBARE_COLUMN+" / 100 = " + codCategorie,null,null,null,null);

        nrIntrebari=cursor.getCount();
        cursor.close();
        return nrIntrebari;
    }
    //date studenti pt raport
    public ArrayList<Student> getDateStudenti ()
    {
        ArrayList<Student> studenti = new ArrayList<>();

        Cursor cursor = db.query(DenumiriBD.TABELA_Studenti,
                new String[] {DenumiriBD.USERNAME_STUD_COLUMN, DenumiriBD.CNP_COLUMN, DenumiriBD.GRUPA_COLUMN,DenumiriBD.SERIA_COLUMN},
                null,null,null,null,null);

        if(cursor!=null && cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            do {
                studenti.add(new Student(cursor.getString(0),cursor.getString(1),cursor.getString(2),
                        cursor.getString(3)));
            }while (cursor.moveToNext());
        }
        cursor.close();
        return studenti;
    }

}
