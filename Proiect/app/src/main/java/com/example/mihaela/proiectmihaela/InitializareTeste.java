package com.example.mihaela.proiectmihaela;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class InitializareTeste {

    public class GetIntrebari extends AsyncTask<String, Void, ArrayList<Intrebare>>
    {

        @Override
        protected ArrayList<Intrebare> doInBackground(String... strings) {
            ArrayList<Intrebare> intrebariList = new ArrayList<>();
            StringBuilder builder= new StringBuilder();
            try{
                URL url = new URL(strings[0]);
                HttpURLConnection http = (HttpURLConnection)url.openConnection();
                InputStream stream = http.getInputStream();

                InputStreamReader istrr = new InputStreamReader(stream);
                BufferedReader reader = new BufferedReader(istrr);
                String linie="";
                while((linie=reader.readLine())!=null)
                {
                    builder.append(linie);
                }

                JSONArray vector = new JSONArray(builder.toString());
                for(int i=0; i<vector.length(); i++)
                {
                    JSONObject obiect = (JSONObject)vector.get(i);
                    int idTest = obiect.getInt("idTest");
                    String intrebare = obiect.getString("intrebare");

                    JSONArray raspunsuriVect = obiect.getJSONArray("raspunsuri");
                    ArrayList<String> raspunsuri = new ArrayList<>();
                    for(int j=0; j<4; j++)
                    {
                        String rasp = raspunsuriVect.get(j).toString();
                        raspunsuri.add(rasp);
                    }
                    int idCorect = obiect.getInt("indexCorect");

                    intrebariList.add(new Intrebare(idTest, intrebare,
                            raspunsuri.get(0),raspunsuri.get(1), raspunsuri.get(2), raspunsuri.get(3),idCorect));

                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return intrebariList;
        }
    }

    private ArrayList<Intrebare> intrebariTest = new ArrayList<>();
    private ArrayList<Test> teste = new ArrayList<>();

    public ArrayList<Test> getTest() {
        return teste;
    }

    public InitializareTeste() {
        //colectare date in mod asincron
        GetIntrebari obiect = new GetIntrebari(){
            @Override
            protected void onPostExecute(ArrayList<Intrebare> intrebari) {

            }
        };

        obiect.execute("https://api.myjson.com/bins/wqs88");
        try {
            //obtinem vectorul de teste(intrebari+raspunsuri)
            this.intrebariTest = obiect.get();
            //formam testele be baza intrebarilor primite
            this.teste = new ArrayList<>();
            teste.add(new Test(this.intrebariTest.get(0).getIdIntrebare(),5,this.intrebariTest.subList(0,3),10,"Generalitati C#"));
            teste.add(new Test(this.intrebariTest.get(5).getIdIntrebare(),5,this.intrebariTest.subList(4,9),10,"Generalitati JAVA"));

        } catch (ExecutionException e) {
            e.printStackTrace();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
