package com.example.mihaela.proiectmihaela;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class RaportSituatieStudActivity extends AppCompatActivity {
    StringBuilder sb =new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raport_situatie_stud);

        //studentii din BD
        AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
        adapter.deschideConexiuneBD();
        ArrayList<Student> studenti = adapter.getDateStudenti();

        ArrayList<Utilizator> dateDinUser = new ArrayList<>();
        for(Student stud : studenti){
          dateDinUser.add(adapter.getUserByUsername(stud.getUsername()));
        }
        adapter.inchideConexiuneBD();

        sb.append("RAPORT SITUATIE STUDENTI CARE FOLOSESC APLICATIA");
        sb.append("\n\n");
        for(int i=0 ; i< studenti.size(); i++){
            sb.append("Studentul: "+dateDinUser.get(i).getNumePrenume()+"\n");
            sb.append("CNP: "+studenti.get(0).getCnp()+"\n");
            sb.append("Facultate: "+dateDinUser.get(0).getFacultate()+"\n");
            sb.append("Grupa: "+studenti.get(0).getGrupa()+"\n");
            sb.append("**************************");
            sb.append("\n");
        }
        TextView raportTv = findViewById(R.id.dateRaportStudentiTV);
        raportTv.setText(sb.toString());
    }

    public void salveazaRaport(View view) {
    }
}
