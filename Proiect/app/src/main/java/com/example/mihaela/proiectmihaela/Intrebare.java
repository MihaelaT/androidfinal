package com.example.mihaela.proiectmihaela;

import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.Space;

public class Intrebare implements Parcelable {
    private int idIntrebare;
    private String intrebare;
    private String raspuns1;

    public int getIdCorect() {
        return idCorect;
    }

    public void setIdCorect(int idCorect) {
        this.idCorect = idCorect;
    }

    private String raspuns2;
    private String raspuns3;
    private String raspuns4;
    private int idCorect;

    public Intrebare(int idIntrebare, String intrebare, String raspuns1, String raspuns2, String raspuns3, String raspuns4, int idCorect) {
        this.idIntrebare = idIntrebare;
        this.intrebare = intrebare;
        this.raspuns1 = raspuns1;
        this.raspuns2 = raspuns2;
        this.raspuns3 = raspuns3;
        this.raspuns4 = raspuns4;
        this.idCorect = idCorect;
    }

    protected Intrebare(Parcel in) {
        idIntrebare = in.readInt();
        intrebare = in.readString();
        raspuns1 = in.readString();
        raspuns2 = in.readString();
        raspuns3 = in.readString();
        raspuns4 = in.readString();
        idCorect = in.readInt();
    }

    public static final Creator<Intrebare> CREATOR = new Creator<Intrebare>() {
        @Override
        public Intrebare createFromParcel(Parcel in) {
            return new Intrebare(in);
        }

        @Override
        public Intrebare[] newArray(int size) {
            return new Intrebare[size];
        }
    };

    public int getIdIntrebare() {
        return idIntrebare;
    }

    public void setIdIntrebare(int idIntrebare) {
        this.idIntrebare = idIntrebare;
    }

    public String getIntrebare() {
        return intrebare;
    }

    public void setIntrebare(String intrebare) {
        this.intrebare = intrebare;
    }

    public String getRaspuns1() {
        return raspuns1;
    }

    public void setRaspuns1(String raspuns1) {
        this.raspuns1 = raspuns1;
    }

    public String getRaspuns2() {
        return raspuns2;
    }

    public void setRaspuns2(String raspuns2) {
        this.raspuns2 = raspuns2;
    }

    public String getRaspuns3() {
        return raspuns3;
    }

    public void setRaspuns3(String raspuns3) {
        this.raspuns3 = raspuns3;
    }

    public String getRaspuns4() {
        return raspuns4;
    }

    public void setRaspuns4(String raspuns4) {
        this.raspuns4 = raspuns4;
    }


    public void setCorect(int idCorect) {
        this.idCorect = idCorect;
    }

    @Override
    public String toString() {
        return "Intrebarea: "+this.getIntrebare()+"\n"+
                "raspuns1: "+this.getRaspuns1() + "\n" +
                "raspuns2: "+this.getRaspuns2() + "\n" +
                "raspuns3: "+this.getRaspuns3() + "\n" +
                "raspuns4: "+this.getRaspuns4() + "\n";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idIntrebare);
        dest.writeString(intrebare);
        dest.writeString(raspuns1);
        dest.writeString(raspuns2);
        dest.writeString(raspuns3);
        dest.writeString(raspuns4);
        dest.writeInt(idCorect);
    }
}
