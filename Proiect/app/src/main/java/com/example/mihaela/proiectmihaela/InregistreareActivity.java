package com.example.mihaela.proiectmihaela;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.Toolbar;

public class InregistreareActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inregistrare);


        ImageButton omuletBtn=findViewById(R.id.omuletImgBtn);
        omuletBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ProfilActivity.class);
                startActivity(intent);
            }
        });
        ImageButton offBtn=findViewById(R.id.offImgBtn);
        offBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),FeedBackActivity.class);
                startActivity(intent);
            }
        });
        ImageButton setariBtn=findViewById(R.id.setariImgBtn);
        setariBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),GestionareTesteActivity.class);
                startActivity(intent);
            }
        });

    }

    //metoda de creare user
    public void registerUser(View view) {
        //colectam datele introduse de utilizator
        EditText numePrenumeET = findViewById(R.id.numeEditIndregistrare);
        EditText emailET = findViewById(R.id.emailEditInregistrare);
        EditText usernameET = findViewById(R.id.userEditInregistrare);
        EditText parolaET = findViewById(R.id.parolaEditInregistrare);
        Spinner facultateSP = findViewById(R.id.facultateSpinner);
        Spinner tipSP = findViewById(R.id.tipSpinner);

        String numePrenume = numePrenumeET.getText().toString();
        String email = emailET.getText().toString();
        String username = usernameET.getText().toString();
        String parola = parolaET.getText().toString();
        String facultate = facultateSP.getSelectedItem().toString();
        String tip = tipSP.getSelectedItem().toString();

        //daca utilizatorul a introdus toate datele -> se insereaza in baza de date
        if(!numePrenume.isEmpty() && !email.isEmpty() && !username.isEmpty() && !parola.isEmpty())
        {
            final Utilizator utilizator = new Utilizator(numePrenume,email,username,parola,facultate,tip,0);
            AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
            adapter.deschideConexiuneBD();

            //se verifica cazul in care utilizatorul deja exista in baza de date
            if(adapter.existaUser(username) == false) {
                //nu exista userul - deci se insereaza in BD
                adapter.inserareUtilizatori(utilizator);

                //daca utilizatorul este student -> acesta se introduce si in tabela STUDENTI
                if(utilizator.getTipCont().equals("Student")){
                 Student student = new Student((utilizator.getUserName()));
                 adapter.inserareStudent(student);
                }

                adapter.inchideConexiuneBD();
                Toast.makeText(getApplicationContext(), "Utilizator inregistrat cu succes!", Toast.LENGTH_LONG).show();

                //se seteaza vizibil un buton ce permite utilizatorului sa intre in interfata de logare
                Button back = findViewById(R.id.backToLogin_btn);
                back.setVisibility(View.VISIBLE);
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), LogareActivity.class);
                        intent.putExtra("utilizator", utilizator.getNumePrenume());
                        intent.putExtra("parola", utilizator.getParola());
                        startActivity(intent);
                    }
                });
            }
            else{
                //mai exista un utilizator cu username-ul introdus !
                Toast.makeText(getApplicationContext(), "Username deja folosit! Introduceti un nou username (ex:"+username+"123)",
                        Toast.LENGTH_LONG).show();
                usernameET.setText("");
            }
        }
    }
}
