package com.example.mihaela.proiectmihaela;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toolbar;

import java.util.ArrayList;

public class InscriereTestActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inscriere_test);
        Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.meniu);
        ImageButton omuletBtn=findViewById(R.id.omuletImgBtn);
        omuletBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ProfilActivity.class);
                startActivity(intent);
            }
        });
        ImageButton offBtn=findViewById(R.id.offImgBtn);
        offBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),FeedBackActivity.class);
                startActivity(intent);
            }
        });
        ImageButton setariBtn=findViewById(R.id.setariImgBtn);
        setariBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),GestionareTesteActivity.class);
                startActivity(intent);
            }
        });

        //incepem testul
        EditText codTestET = findViewById(R.id.codEdit);
        codTestET.setText(""+getIntent().getIntExtra("codTest",1));
    }

    public void salveazaBtn(View view) {
        InitializareTeste teste = new InitializareTeste();
        ArrayList<Test> rezultat = teste.getTest();
        EditText codTestET = findViewById(R.id.codEdit);
        int cod = Integer.parseInt(codTestET.getText().toString());
//        this.testActual = getIntent().getParcelableExtra("test"); ASTA NE ASTEAPTA IN TEST
        Intent intent = new Intent(getApplicationContext(),TestActivity.class);
        intent.putExtra("test",rezultat.get(cod));
        startActivity(intent);
    }
}
