package com.example.mihaela.proiectmihaela;

import android.os.Parcel;
import android.os.Parcelable;

public class Student implements Parcelable {
    private String username;
    private String cnp;
    private String grupa;
    private String seria;

    public Student() {
    }

    public Student(String username) {
        //avem nevoie de un constructor doar cu PK-ul tabelei Studenti deoarece in ea se insereaza la crearea contului
        //cand nu stim alte date despre user (precum cnp, serie, grupa -> acestea sunt introduse in BD printr-un UPDATE din cadrul profilului)
        this.username = username;
        this.cnp = "introduceti cnp-ul";
        this.grupa = "introduceti grupa";
        this.seria = "C";
    }

    protected Student(Parcel in) {
        username = in.readString();
        cnp = in.readString();
        grupa = in.readString();
        seria = in.readString();
    }

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };

    @Override
    public String toString() {
        return "Student{" +
                "username='" + username + '\'' +
                ", cnp='" + cnp + '\'' +
                ", grupa='" + grupa + '\'' +
                ", seria='" + seria + '\'' +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getGrupa() {
        return grupa;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    public String getSeria() {
        return seria;
    }

    public void setSeria(String seria) {
        this.seria = seria;
    }

    public Student(String username, String cnp, String grupa, String seria) {

        this.username = username;
        this.cnp = cnp;

        this.grupa = grupa;
        this.seria = seria;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(cnp);
        dest.writeString(grupa);
        dest.writeString(seria);
    }
}
