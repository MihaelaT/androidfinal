package com.example.mihaela.proiectmihaela;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

public class CreareTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creare_test);

        Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.meniu);
        ImageButton omuletBtn=findViewById(R.id.omuletImgBtn);
        omuletBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ProfilActivity.class);
                startActivity(intent);
            }
        });
        ImageButton offBtn=findViewById(R.id.offImgBtn);
        offBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),FeedBackActivity.class);
                startActivity(intent);
            }
        });
        ImageButton setariBtn=findViewById(R.id.setariImgBtn);
        setariBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),GestionareTesteActivity.class);
                startActivity(intent);
            }
        });

    }

    public void act8_adaugaClick(View view) {

        //preluam datele aferente unei intrebari pentru introducerea in BD
        EditText intrebareET = findViewById(R.id.inputIntrebareEdit);
        EditText rasp1ET = findViewById(R.id.raspuns1EditCreare);
        EditText rasp2ET = findViewById(R.id.raspuns2EditCreare);
        EditText rasp3ET = findViewById(R.id.raspuns3EditCreare);
        EditText rasp4ET = findViewById(R.id.raspuns4EditCreare);
        EditText categorieIntrebare = findViewById(R.id.categorieET);

        CheckBox rasp1CB = findViewById(R.id.rCorectchkb1);
        CheckBox rasp2CB = findViewById(R.id.rCorectchb2);
        CheckBox rasp3CB = findViewById(R.id.rCorectchb3);
        CheckBox rasp4CB = findViewById(R.id.rCorectchb4);

        //verificam daca doar UN SINGUR CheckBox a fost bifat
        int raspuns_corect = 0; int ok = 0;
        if(rasp1CB.isChecked()) {
            raspuns_corect = 1;
            ok++;
        }
         if(rasp2CB.isChecked()){
            raspuns_corect = 2;
            ok++;
        }
        if(rasp3CB.isChecked()){
            raspuns_corect = 3;
            ok++;
        }
        if(rasp4CB.isChecked()){
            raspuns_corect = 4;
            ok++;
        }
         if(ok != 1) //ok o sa fie 1 doar daca un singur cb a fost bifat (0 daca niciunul, alta valoare in f de cate s-au bifat)
         {
             Toast toast = Toast.makeText(getApplicationContext(),
                     "Intrebarea trebie sa aiba UN SINGUR raspuns corect",Toast.LENGTH_LONG);
             TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
             v.setTextColor(Color.RED);
             toast.show();
         }
         else
             if(intrebareET.getText().toString().isEmpty() ||
                     rasp1ET.getText().toString().isEmpty() ||
                     rasp2ET.getText().toString().isEmpty() ||
                     rasp3ET.getText().toString().isEmpty() ||
                     rasp4ET.getText().toString().isEmpty() ||
                     categorieIntrebare.getText().toString().isEmpty())
             {
                 //daca unul din campuri este null atunci nu adaugam in BD
                 Toast toast = Toast.makeText(getApplicationContext(),
                         "Completati toate campurile !!!",Toast.LENGTH_LONG);
                 TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
                 v.setTextColor(Color.RED);
                 toast.show();
             }
             else {
            //totul este completat -> introducem intrebarea in BD
                 //idIntrebare este format din categorie + nr de intrebari existente in BD
                 AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
                 adapter.deschideConexiuneBD();
                 //vedem cate intrebari exista in BD
                 int nrIntr = adapter.selectCountIntrebare();
                 // forma id-ului: categ_0_nr intrebari existente (ex: 100 - prima intrebare  din categoria 1 )
                 int idIntrebare = Integer.parseInt(categorieIntrebare.getText().toString()) * 100 + nrIntr;

                 Intrebare intrebare = new Intrebare(idIntrebare,intrebareET.getText().toString(),
                         rasp1ET.getText().toString(), rasp2ET.getText().toString(),
                         rasp3ET.getText().toString(),rasp4ET.getText().toString(),raspuns_corect);
                 //inseram intrebarea utilizatorului in BD
                 adapter.inserareIntrebare(intrebare);
                 Toast.makeText(getApplicationContext(),
                         "Intrebare adaugata cu succes!",Toast.LENGTH_LONG).show();

                 adapter.inchideConexiuneBD();
             }

    }

    public void act8_stergeClick(View view) {
        EditText intrebareET = findViewById(R.id.inputIntrebareEdit);
        EditText rasp1ET = findViewById(R.id.raspuns1EditCreare);
        EditText rasp2ET = findViewById(R.id.raspuns2EditCreare);
        EditText rasp3ET = findViewById(R.id.raspuns3EditCreare);
        EditText rasp4ET = findViewById(R.id.raspuns4EditCreare);
        EditText categorieIntrebare = findViewById(R.id.categorieET);

        if(intrebareET.getText().toString().isEmpty())
        {
            Toast toast = Toast.makeText(getApplicationContext(),"Introduceti textul intrebarii pentru care vreti sa stergeti raspunsurile!!",
                    Toast.LENGTH_LONG);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(Color.RED);
            toast.show();
        }
        else{
            //stergem intrebarea din BD
            AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
            adapter.deschideConexiuneBD();
            adapter.deleteIntrebare(intrebareET.getText().toString());
            //ii spunem utilizatoruli ca intrebarea a fost stearsa cu succes si afisam mesajul cu verde ^.^
            Toast toast = Toast.makeText(getApplicationContext(),"Intrebarea a fost stearsa cu succes!!",
                    Toast.LENGTH_LONG);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(Color.GREEN);
            toast.show();

            intrebareET.setText("");
            rasp1ET.setText(""); rasp2ET.setText(""); rasp3ET.setText(""); rasp4ET.setText("");
            adapter.inchideConexiuneBD();
        }
    }

    public void modificaCreare(View view) {

    }
}
