package com.example.mihaela.proiectmihaela;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import java.util.ArrayList;

public class DesenareGrafice extends View {
    ArrayList<Float> date = new ArrayList<>();
    String tipGrafic;

    public DesenareGrafice(Context context,  ArrayList<Float> date, String tipGrafic) {
        super(context);
        this.date = date;
        this.tipGrafic = tipGrafic;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p = new Paint();
        int plecare=0;

        for(int i=0 ; i<this.date.size(); i++)
        {
            p.setColor(Color.rgb(i*50,i*5+1,i*7));
            canvas.drawArc(100,100,800,800,plecare,this.date.get(i),true,p);
            plecare+=this.date.get(i);
        }
        //normalizarea se face in functie de maxim la bar chart ex/; suma echivalat cum maximul si regula de 3s

    }
}
