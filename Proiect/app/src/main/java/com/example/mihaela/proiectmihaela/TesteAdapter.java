package com.example.mihaela.proiectmihaela;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class TesteAdapter extends ArrayAdapter<Test>
{
    private int layout;
    private Context context;

    public TesteAdapter(Context context, int resource, List<Test> objects) {
        super(context, resource, objects);
        layout = resource;
        this.context=context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Test test = getItem(position);
        LayoutInflater inflater = LayoutInflater.from(this.context);
        View item = inflater.inflate(layout,null);

        ImageView imagine = item.findViewById(R.id.imagAdaptorTest);
        if(test.getIdTest() == 1)
            imagine.setImageResource(R.drawable.testunu);
        else
        if (test.getIdTest() == 2)
            imagine.setImageResource(R.drawable.testdoi);
        else
        if (test.getIdTest() == 3)
            imagine.setImageResource(R.drawable.testtrei);
        else
        if (test.getIdTest()==4)
            imagine.setImageResource(R.drawable.testpatru);
        else
        if (test.getIdTest() == 5)
            imagine.setImageResource(R.drawable.testcinci);

        TextView titlu = item.findViewById(R.id.titluTest_TV);
        titlu.setText(test.getNumeTest());
        TextView nrIntrebari = item.findViewById(R.id.nrIntrebTest_CTV);
        nrIntrebari.setText("Nr intrebari: "+test.getNrIntrebari());

        return item;
    }
}
