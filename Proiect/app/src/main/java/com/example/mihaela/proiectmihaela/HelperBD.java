package com.example.mihaela.proiectmihaela;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class HelperBD extends SQLiteOpenHelper {
    public HelperBD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //creare tabela Utilizator
        //USERNAME_COLUMN este PK
        //ID_SITUATIE_U_COLUMN este FK
        db.execSQL("create table "+DenumiriBD.TABELA_UTILIZATOR+"("+
                        DenumiriBD.NUME_PRENUME_COLUMN+" text not null, "+
                        DenumiriBD.EMAIL_COLUMN+" text not null, "+
                        DenumiriBD.USERNAME_COLUMN+" text not null primary key, "+
                        DenumiriBD.PAROLA_COLUMN+" text not null, "+
                        DenumiriBD.FACULTATE_COLUMN+" text not null, "+
                        DenumiriBD.TIP_CONT_COLUMN+" text not null, "+
                        DenumiriBD.ID_SITUATIE_U_COLUMN+
                        " integer, "+
                        " foreign key ("+DenumiriBD.ID_SITUATIE_U_COLUMN+
                        ") references "+DenumiriBD.TABELA_SITUATIE_U+
                        "("+DenumiriBD.ID_SITUATIE_S_COLUMN+"));");

        //creare tabela Test
        //ID_TEST_COLUMN est PK
        //ID_INTREBARE_T_COLUMN este FK
        db.execSQL("create table "+DenumiriBD.TABELA_TEST+"("+
                DenumiriBD.ID_TEST_COLUMN+" integer primary key, "+
                DenumiriBD.NR_INTREBARI_COLUMN+" integer, "+
                DenumiriBD.ID_INTREBARE_T_COLUMN+" integer, "+
                DenumiriBD.NOTA_COLUMN+" real, "+
                DenumiriBD.NUME_TEST_COLUMN+" text not null, " +
                " foreign key ("+DenumiriBD.ID_INTREBARE_T_COLUMN+
                ") references "+DenumiriBD.TABELA_INTREBARE+
                "("+DenumiriBD.ID_INTREBARE_COLUMN+"));");

        //creare tabela Intrebare
        //ID_INTREBARE_COLMUN este PK
        db.execSQL("create table "+DenumiriBD.TABELA_INTREBARE+"("+
                DenumiriBD.ID_INTREBARE_COLUMN+" integer primary key, "+
                DenumiriBD.INTREBARE_COLUMN+" text not null, "+
                DenumiriBD.RASPUNS1_COLUMN+" text not null, "+
                DenumiriBD.RASPUNS2_COLUMN+" text not null, "+
                DenumiriBD.RASPUNS3_COLUMN+" text not null, "+
                DenumiriBD.RASPUNS4_COLUMN+" text not null, "+
                DenumiriBD.CORECT_COLUMN+" integer default 0);");

        //creare tabela Situatie Utilizator
        //ID_SITUATIE_S_COLUMN este PK
        db.execSQL("create table "+DenumiriBD.TABELA_SITUATIE_U+"("+
                DenumiriBD.ID_SITUATIE_S_COLUMN+" integer primary key, "+
                DenumiriBD.ID_TEST_S_COLUMN+" integer, "+
                DenumiriBD.SCOR_TOTAL_COLUMN+" real);");

        //creare tabela Studenti
        //USERNAME_STUD_COLUMN este PK
        db.execSQL("create table "+DenumiriBD.TABELA_Studenti+"("+
                DenumiriBD.USERNAME_STUD_COLUMN+" text primary key, "+
                DenumiriBD.CNP_COLUMN+" text not null, "+
                DenumiriBD.GRUPA_COLUMN+" text not null, "+
                DenumiriBD.SERIA_COLUMN+" text not null);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists "+DenumiriBD.TABELA_UTILIZATOR);
        db.execSQL("drop table if exists "+DenumiriBD.TABELA_TEST);
        db.execSQL("drop table if exists "+DenumiriBD.TABELA_INTREBARE);
        db.execSQL("drop table if exists "+DenumiriBD.TABELA_SITUATIE_U);
        db.execSQL("drop table if exists "+DenumiriBD.TABELA_Studenti);
        onCreate(db);

    }
}
