package com.example.mihaela.proiectmihaela;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class ProfilProfesorActivity extends AppCompatActivity {
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profil_profesor);

        //primim datele profilului din LOGIN
        this.username= getIntent().getStringExtra("username");

        //pentru a cauta in BD exact utilizatorul al carui profil este afisat
        //primim username-ul din activitatea de LOGIN
        this.username = getIntent().getStringExtra("username");

        AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
        adapter.deschideConexiuneBD();
        Utilizator userInitial = adapter.getUserByUsername(this.username);

        if(userInitial!=null) {
            EditText numePrenumeET = findViewById(R.id.numePrenume_ProfilProf);
            EditText emailET = findViewById(R.id.email_ProfilProf);
            EditText facultateET = findViewById(R.id.facultate_ProfilProf);

            numePrenumeET.setText(userInitial.getNumePrenume());
            emailET.setText(userInitial.getEmail());
            facultateET.setText(userInitial.getFacultate());
        }
    }

    public void editeazaCont(View view) {
        //update pe tabela User si Student DACA ESTE CAZUL!!!

        //date pentru tabela UTILIZATOR
        EditText numePrenumeET = findViewById(R.id.numePrenume_ProfilProf);
        EditText emailET = findViewById(R.id.email_ProfilProf);
        EditText facultateET = findViewById(R.id.facultate_ProfilProf);

        //deschidem conexiunea cu BD
        AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
        adapter.deschideConexiuneBD();

        //ne aducem datele din BD - aferente utilizatoruli actual
        Utilizator user = adapter.getUserByUsername(this.username);

        //verificam daca s-au efectuat modificari pentru tabela UTILIZATOR
        //daca datele introduse de utilizator sunt diferite de cele din tabela (si diferite de null!!!) -> le modificam
        if(!numePrenumeET.getText().toString().isEmpty() && !numePrenumeET.getText().toString().equals(user.getNumePrenume()) ||
                !emailET.getText().toString().isEmpty() && !emailET.getText().toString().equals(user.getEmail()) ||
                !facultateET.getText().toString().isEmpty() && !facultateET.getText().toString().equals(user.getFacultate()))
        {
            //creez un nou utilizator care are atat datele modificate cat si datele vechi care nu se pot modifica
            Utilizator utilizatorModificat = new Utilizator(numePrenumeET.getText().toString(),emailET.getText().toString(),
                    user.getUserName(),user.getParola(),facultateET.getText().toString(),user.getTipCont(),user.getIdSituatie());

            adapter.updateUtilizatori(utilizatorModificat);
            Toast.makeText(getApplicationContext(),"Datele au fost modificate cu succes!",Toast.LENGTH_LONG).show();
            adapter.inchideConexiuneBD();
        }
    }

    public void stergeCont(View view) {
        //daca tot a fost sters contul -> trimitem utilizatorul in activitatea de feedback
        AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
        adapter.deschideConexiuneBD();
        adapter.deleteUtilizatori(this.username);
        adapter.inchideConexiuneBD();
        Intent intent=new Intent(getApplicationContext(),FeedBackActivity.class);
        startActivity(intent);
    }

    public void testeleMele(View view) {
        Intent intent=new Intent(getApplicationContext(),GestionareTesteActivity.class);
        startActivity(intent);
    }
}
