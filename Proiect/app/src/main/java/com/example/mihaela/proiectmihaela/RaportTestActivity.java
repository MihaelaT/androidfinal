package com.example.mihaela.proiectmihaela;

import android.content.Context;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class RaportTestActivity extends AppCompatActivity {
    private StringBuilder sb = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raport_test);

        TextView raportTV = findViewById(R.id.dateRaportTV);
        raportTV.setMovementMethod(new ScrollingMovementMethod()); //pt scroll

        //aducem toate intrebarile din Bd
        AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
        adapter.deschideConexiuneBD();
        ArrayList<Intrebare> intrebari = adapter.selectAllIntrebare();
        adapter.inchideConexiuneBD();
        sb.append("RAPORT SITUATIE TESTE \n");
        sb.append("----------------------------");
        sb.append("\n");
        for (Intrebare intreb : intrebari)
        {
            sb.append(intreb.toString());
            sb.append("----------------------------");
            sb.append("\n");
        }
        raportTV.setText(sb.toString());
    }

    public void salveazaRaport(View view) throws IOException {
        File file = getFileStreamPath("raport.txt");

        if (!file.exists()) {
            file.createNewFile();
        }

        FileOutputStream writer = openFileOutput(file.getName(), Context.MODE_PRIVATE);

        writer.write("merge".getBytes());
        writer.flush();

        writer.close();
        Toast.makeText(getApplicationContext(),"Raport salvat in fisierul \"raportTeste.txt\"",Toast.LENGTH_LONG).show();
    }
}
