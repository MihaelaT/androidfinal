package com.example.mihaela.proiectmihaela;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Test  implements Parcelable {
    private int idTest;
    private int nrIntrebari;
    private List<Intrebare> intrebari;
    private float nota;
    private String numeTest;

    public Test() {
    }

    public Test(int idTest, int nrIntrebari, List<Intrebare> intrebari, float nota, String numeTest) {
        this.idTest = idTest;
        this.nrIntrebari = nrIntrebari;
        this.intrebari = intrebari;
        this.nota = nota;
        this.numeTest = numeTest;
    }

    protected Test(Parcel in) {
        idTest = in.readInt();
        nrIntrebari = in.readInt();
        intrebari = in.createTypedArrayList(Intrebare.CREATOR);
        nota = in.readFloat();
        numeTest = in.readString();
    }

    public static final Creator<Test> CREATOR = new Creator<Test>() {
        @Override
        public Test createFromParcel(Parcel in) {
            return new Test(in);
        }

        @Override
        public Test[] newArray(int size) {
            return new Test[size];
        }
    };

    public int getIdTest() {
        return idTest;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public int getNrIntrebari() {
        return nrIntrebari;
    }

    public void setNrIntrebari(int nrIntrebari) {
        this.nrIntrebari = nrIntrebari;
    }

    public List<Intrebare> getIntrebari() {
        return intrebari;
    }

    public void setIntrebari(List<Intrebare> intrebari) {
        this.intrebari = intrebari;
    }

    public float getNota() {
        return nota;
    }

    public void setNota(float nota) {
        this.nota = nota;
    }

    public String getNumeTest() {
        return numeTest;
    }

    public void setNumeTest(String numeTest) {
        this.numeTest = numeTest;
    }

    @Override
    public String toString() {
        return "Test{" +
                "idTest=" + idTest +
                ", nrIntrebari=" + nrIntrebari +
                ", intrebari=" + intrebari +
                ", nota=" + nota +
                ", numeTest='" + numeTest + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idTest);
        dest.writeInt(nrIntrebari);
        dest.writeTypedList(intrebari);
        dest.writeFloat(nota);
        dest.writeString(numeTest);
    }
}
