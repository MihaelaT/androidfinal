package com.example.mihaela.proiectmihaela;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.ArrayList;

public class TestActivity extends AppCompatActivity {

    private Test testActual = new Test();
    private ArrayList<Integer> raspunsuriCorecte = new ArrayList<>();
    private ArrayList<Integer> raspunsuriUtilizator = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);

        Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.meniu);
        ImageButton omuletBtn=findViewById(R.id.omuletImgBtn);
        omuletBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ProfilActivity.class);
                startActivity(intent);
            }
        });
        ImageButton offBtn=findViewById(R.id.offImgBtn);
        offBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),FeedBackActivity.class);
                startActivity(intent);
            }
        });
        ImageButton setariBtn=findViewById(R.id.setariImgBtn);
        setariBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),GestionareTesteActivity.class);
                startActivity(intent);
            }
        });

        //luam testul pe care trebuie sa il pornim din intent
        this.testActual = getIntent().getParcelableExtra("test");
        for(Intrebare intrebare : this.testActual.getIntrebari()){
            this.raspunsuriCorecte.add(intrebare.getIdCorect());//aflam raspunsurile corecte
            this.raspunsuriUtilizator.add(0); //pregatim vectorul de raspunsuri utilizator - > indexul e nr itrebarii, valoarea e raspunsul
        }
        SeekBar indexSB = findViewById(R.id.seekBar);
        indexSB.setMax(this.testActual.getNrIntrebari()-1);

        TextView intrebareTV = findViewById(R.id.intrebareTxtTest); //initializam interfata cu prima intrebare
        intrebareTV.setText(this.testActual.getIntrebari().get(0).getIntrebare());
        RadioButton rb1 = findViewById(R.id.rb1);
        rb1.setText(this.testActual.getIntrebari().get(0).getRaspuns1());
        RadioButton rb2 = findViewById(R.id.rb2);
        rb2.setText(this.testActual.getIntrebari().get(0).getRaspuns2());
        RadioButton rb3 = findViewById(R.id.rb3);
        rb3.setText(this.testActual.getIntrebari().get(0).getRaspuns3());
        RadioButton rb4 = findViewById(R.id.rb4);
        rb4.setText(this.testActual.getIntrebari().get(0).getRaspuns4());
    }
    public void clickRaspuns(View view) {
       //radiobtn
    }

    public void goBackBTN(View view) {
        SeekBar indexSB = findViewById(R.id.seekBar);
        int seekValue = indexSB.getProgress(); //aflam la ce intrebare se afla utiliaztorul
        if(seekValue!= 0) {
            seekValue--;
            indexSB.setProgress(seekValue);

            //afisam intrebarea
            TextView intrebareTV = findViewById(R.id.intrebareTxtTest);
            intrebareTV.setText(this.testActual.getIntrebari().get(seekValue).getIntrebare());
            RadioButton rb1 = findViewById(R.id.rb1);
            rb1.setText(this.testActual.getIntrebari().get(seekValue).getRaspuns1());
            RadioButton rb2 = findViewById(R.id.rb2);
            rb2.setText(this.testActual.getIntrebari().get(seekValue).getRaspuns2());
            RadioButton rb3 = findViewById(R.id.rb3);
            rb3.setText(this.testActual.getIntrebari().get(seekValue).getRaspuns3());
            RadioButton rb4 = findViewById(R.id.rb4);
            rb4.setText(this.testActual.getIntrebari().get(seekValue).getRaspuns4());
        }
    }

    public void nextBtn(View view) {
        SeekBar indexSB = findViewById(R.id.seekBar);
        int seekValue = indexSB.getProgress(); //aflam la ce intrebare se afla utiliaztorul
        if(seekValue< this.testActual.getNrIntrebari()-1) {
            seekValue++;
            indexSB.setProgress(seekValue);
            //afisam intrebarea
            TextView intrebareTV = findViewById(R.id.intrebareTxtTest);
            intrebareTV.setText(this.testActual.getIntrebari().get(seekValue).getIntrebare());
            RadioButton rb1 = findViewById(R.id.rb1);
            rb1.setText(this.testActual.getIntrebari().get(seekValue).getRaspuns1());
            RadioButton rb2 = findViewById(R.id.rb2);
            rb2.setText(this.testActual.getIntrebari().get(seekValue).getRaspuns2());
            RadioButton rb3 = findViewById(R.id.rb3);
            rb3.setText(this.testActual.getIntrebari().get(seekValue).getRaspuns3());
            RadioButton rb4 = findViewById(R.id.rb4);
            rb4.setText(this.testActual.getIntrebari().get(seekValue).getRaspuns4());
        }
    }

    public void clickTerminaTest(View view) {
        //calculam nota
        float punctajIntrebare = 10/this.raspunsuriUtilizator.size(); // impartim la cate intrebari avem
        float nota=0f;
        int nrIntrebariGresite=0;
        for(int i = 0; i < this.raspunsuriCorecte.size(); i++)
        {
            if(this.raspunsuriCorecte.get(i) == this.raspunsuriUtilizator.get(i))
                nota+=punctajIntrebare;
            else
                nrIntrebariGresite++;
        }
        Intent intent = new Intent(getApplicationContext(),RezultatTestActivity.class);
        intent.putExtra("nota",nota);
        intent.putExtra("nrGresite",nrIntrebariGresite);
        startActivity(intent);
    }

    public void salveazaRaspuns(View view) {
        //se salveaza raspunsul pentru intrebarea curenta
        SeekBar indexSB = findViewById(R.id.seekBar);
        int seekValue = indexSB.getProgress(); //aflam la ce intrebare se afla utiliaztorul
        if(seekValue >= 0 && seekValue < this.testActual.getNrIntrebari())
        {
            RadioButton rb1 = findViewById(R.id.rb1);
            RadioButton rb2 = findViewById(R.id.rb2);
            RadioButton rb3 = findViewById(R.id.rb3);
            RadioButton rb4 = findViewById(R.id.rb4);

            if(rb1.isChecked())
                this.raspunsuriUtilizator.set(seekValue,1);
            if(rb2.isChecked())
                this.raspunsuriUtilizator.set(seekValue,2);
            if(rb3.isChecked())
                this.raspunsuriUtilizator.set(seekValue,3);
            if(rb4.isChecked())
                this.raspunsuriUtilizator.set(seekValue,4);

            Toast.makeText(getApplicationContext(),"Raspuns adaugat!",Toast.LENGTH_LONG).show();
        }
    }
}
