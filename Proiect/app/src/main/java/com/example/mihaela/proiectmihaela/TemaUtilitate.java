package com.example.mihaela.proiectmihaela;

import android.app.Activity;
import android.content.Intent;

public class TemaUtilitate {
    private static int schimbareTema;


    public final static int Theme_Background_alb = 0;
    public final static int Theme_Background_albastru = 1;
    public final static int Theme_Background_galben = 2;

    public static void changeToTheme(Activity activity, int tema) {
        schimbareTema = tema;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
        activity.overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);
    }

    public static void onActivityCreateSetTheme(Activity activity) {
        switch (schimbareTema) {
            default:
            case Theme_Background_alb:
                activity.setTheme(R.style.Theme_Background_alb);
                break;
            case Theme_Background_albastru:
                activity.setTheme(R.style.Theme_Background_albastru);
                break;
            case Theme_Background_galben:
                activity.setTheme(R.style.Theme_Background_galben);
                break;
        }
    }

}
