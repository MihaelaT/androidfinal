package com.example.mihaela.proiectmihaela;

import android.os.Parcel;
import android.os.Parcelable;

public class Utilizator implements Parcelable {
    private String numePrenume;
    private String email;
    private String userName; //PK

    public Utilizator() {
    }

    private String parola;
    private String facultate;
    private String tipCont;
    private int idSituatie;//FK

    public Utilizator(String numePrenume, String email, String userName, String parola, String facultate, String tipCont, int idSituatie) {
        this.numePrenume = numePrenume;
        this.email = email;
        this.userName = userName;
        this.parola = parola;
        this.facultate = facultate;
        this.tipCont = tipCont;
        this.idSituatie = idSituatie;
    }

    protected Utilizator(Parcel in) {
        numePrenume = in.readString();
        email = in.readString();
        userName = in.readString();
        parola = in.readString();
        facultate = in.readString();
        tipCont = in.readString();
        idSituatie = in.readInt();
    }

    public static final Creator<Utilizator> CREATOR = new Creator<Utilizator>() {
        @Override
        public Utilizator createFromParcel(Parcel in) {
            return new Utilizator(in);
        }

        @Override
        public Utilizator[] newArray(int size) {
            return new Utilizator[size];
        }
    };

    public String getNumePrenume() {
        return numePrenume;
    }

    public void setNumePrenume(String numePrenume) {
        this.numePrenume = numePrenume;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }
    //tb acest set avand in vedere ca e PK username
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    public String getFacultate() {
        return facultate;
    }

    public void setFacultate(String facultate) {
        this.facultate = facultate;
    }

    public String getTipCont() {
        return tipCont;
    }

    public void setTipCont(String tipCont) {
        this.tipCont = tipCont;
    }

    public int getIdSituatie() {
        return idSituatie;
    }
    //dar acest set tb?
    public void setIdSituatie(int idSituatie) {
        this.idSituatie = idSituatie;
    }

    @Override
    public String toString() {
        return "Utilizator{" +
                "numePrenume='" + numePrenume + '\'' +
                ", email='" + email + '\'' +
                ", userName='" + userName + '\'' +
                ", parola='" + parola + '\'' +
                ", facultate='" + facultate + '\'' +
                ", tipCont='" + tipCont + '\'' +
                ", idSituatie=" + idSituatie +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(numePrenume);
        dest.writeString(email);
        dest.writeString(userName);
        dest.writeString(parola);
        dest.writeString(facultate);
        dest.writeString(tipCont);
        dest.writeInt(idSituatie);
    }
}
