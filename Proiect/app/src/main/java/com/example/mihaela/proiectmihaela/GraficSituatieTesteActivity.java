package com.example.mihaela.proiectmihaela;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

public class GraficSituatieTesteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafic_situatie_teste);

        float suma=0f;
        float x=0f;

        AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
        adapter.deschideConexiuneBD();
       //categorie c# get nr intrebari din BD
        int nrCateg1 = adapter.countIntrebariCateg(1);
        int nrCateg2 = adapter.countIntrebariCateg(2);
        int nrCateg3 = adapter.countIntrebariCateg(3);
        int nrCateg4 = adapter.countIntrebariCateg(4);

        suma = nrCateg1+nrCateg2+nrCateg3+nrCateg4;
        ArrayList<Float> valori = new ArrayList<>();
        valori.add((nrCateg1*360)/suma);
        valori.add((nrCateg2*360)/suma);
        valori.add((nrCateg3*360)/suma);
        valori.add((nrCateg4*360)/suma);

        DesenareGrafice desen = new DesenareGrafice(getApplicationContext(),valori, " pieChart");
        setContentView(desen);
    }
}
