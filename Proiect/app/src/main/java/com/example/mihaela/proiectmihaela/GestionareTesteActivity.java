package com.example.mihaela.proiectmihaela;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.ArrayList;

public class GestionareTesteActivity extends AppCompatActivity {

       public static Test test = new Test();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gestionare_teste);

        Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.meniu);
        ImageButton omuletBtn=findViewById(R.id.omuletImgBtn);
        omuletBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ProfilActivity.class);
                startActivity(intent);
            }
        });
        ImageButton offBtn=findViewById(R.id.offImgBtn);
        offBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),FeedBackActivity.class);
                startActivity(intent);
            }
        });
        ImageButton setariBtn=findViewById(R.id.setariImgBtn);
        setariBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),GestionareTesteActivity.class);
                startActivity(intent);
            }
        });

        ImageButton creeazaBtn=findViewById(R.id.creeazaImgBtn);
        creeazaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),CreareTestActivity.class);
                startActivity(intent);
            }
        });

        ImageButton rapoarteBtn=findViewById(R.id.rapoarteImgBtn);
        rapoarteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),RapoarteActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.meniu,menu);
        return true;
    }

    //se afiseaza in lista testele create de profesor
    public void generareListaTeste(View view) {
        //se aduc toate intrebarile din BD
        AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
        adapter.deschideConexiuneBD();
        ArrayList<Intrebare> intrebari = adapter.selectAllIntrebare();
        if(intrebari.size() > 0) {
            ArrayList<Intrebare> categ1 = new ArrayList<>();
            ArrayList<Intrebare> categ2 = new ArrayList<>();
            ArrayList<Intrebare> categ3 = new ArrayList<>();
            ArrayList<Intrebare> categ4 = new ArrayList<>();
            ArrayList<Intrebare> categ5 = new ArrayList<>();
            for (Intrebare intr : intrebari) {
                if (intr.getIdIntrebare() / 100 == 1)//ramane din in doar prima cifra = cifra reprezinta categoria
                {
                    categ1.add(intr);
                } else if (intr.getIdIntrebare() / 100 == 2) {
                    categ2.add(intr);
                } else if (intr.getIdIntrebare() / 100 == 3) {
                    categ3.add(intr);
                } else if (intr.getIdIntrebare() / 100 == 4) {
                    categ4.add(intr);
                } else if (intr.getIdIntrebare() / 100 == 5) {
                    categ5.add(intr);
                }
            }

            //se creeaza cate un test pentru fiecare categorie de intrebari
            Test test1 = new Test(1, categ1.size(), categ1, 0, "Test C# nou");
            Test test2 = new Test(2, categ2.size(), categ2, 0, "Test JAVA nou");
            Test test3 = new Test(3, categ3.size(), categ3, 0, "Test JAVASCRIPT nou");
            Test test4 = new Test(4, categ4.size(), categ4, 0, "Test HTML&CSS nou");
            Test test5 = new Test(5, categ5.size(), categ5, 0, "Test C&C++ nou");

            ArrayList<Test> teste = new ArrayList<>(); //adaug in lista de teste doar acele teste dintr-o categorie cu intrebari existente
            if (categ1.size() != 0) teste.add(test1);
            if (categ2.size() != 0) teste.add(test2);
            if (categ3.size() != 0) teste.add(test3);
            if (categ4.size() != 0) teste.add(test4);
            if (categ5.size() != 0) teste.add(test5);

            TesteAdapter adaptor = new TesteAdapter(getApplicationContext(), R.layout.layout_teste_adaptor, teste);
            ListView listaTeste_lv = findViewById(R.id.teste_gestionare_ListV);
            listaTeste_lv.setAdapter(adaptor);
            listaTeste_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    // obtinem testul selectat
                    GestionareTesteActivity.test = (Test) adapterView.getItemAtPosition(i);
                }
            });
        }
    }

    public void incepeTest(View view) {
        //se porneste activitatea de test pentru testul selectat
        Intent intent = new Intent(getApplicationContext(),TestActivity.class);
        intent.putExtra("test",GestionareTesteActivity.test);
        startActivity(intent);
    }

}
