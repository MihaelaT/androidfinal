package com.example.mihaela.proiectmihaela;

public class DenumiriBD {
    final static String NUME_BD="APLICATIE_QUIZZ";

    //constante pentru Utilizator
    final static String TABELA_UTILIZATOR="Utilizator";
    final static String NUME_PRENUME_COLUMN="numePrenume";
    final static String EMAIL_COLUMN="email";
    final static String USERNAME_COLUMN="username";//PK
    final static String PAROLA_COLUMN="parola";
    final static String FACULTATE_COLUMN="facultate";
    final static String TIP_CONT_COLUMN="tipCont";
    final static String ID_SITUATIE_U_COLUMN="idSituatieUt";//aici e FK

    //constante pentru Test
    final static String TABELA_TEST="Test";
    final static String ID_TEST_COLUMN="idTest";
    final static String NR_INTREBARI_COLUMN="nrIntrebari";
    final static String ID_INTREBARE_T_COLUMN="idIntrebare";//aici e FK
    final static String NOTA_COLUMN="nota";
    final static String NUME_TEST_COLUMN="numeTest";

    //constante pentru Intrebare
    final static String TABELA_INTREBARE="Intrebare";
    final static String ID_INTREBARE_COLUMN="idIntrebare"; //aici e PK
    final static String INTREBARE_COLUMN="intrebare";
    final static String RASPUNS1_COLUMN="raspuns1";
    final static String RASPUNS2_COLUMN="raspuns2";
    final static String RASPUNS3_COLUMN="raspuns3";
    final static String RASPUNS4_COLUMN="raspuns4";
    final static String CORECT_COLUMN="corect";


    //constante pentru Situatie
    final static String TABELA_SITUATIE_U="SituatieUtilizator";
    final static String ID_SITUATIE_S_COLUMN="idSituatieSit"; //aici e PK
    final static String ID_TEST_S_COLUMN="idTest";//FK din teste
    final static String SCOR_TOTAL_COLUMN="scorTotalUtilizator";

    //constante pentru Studenti
    final static String TABELA_Studenti="Studenti";
    final static String USERNAME_STUD_COLUMN="username"; //aici e PK - este acelasi username ca din tabela Utilizator
    final static String CNP_COLUMN="CNP";
    final static String GRUPA_COLUMN="grupa";
    final static String SERIA_COLUMN="seria";

}
