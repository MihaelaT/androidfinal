package com.example.mihaela.proiectmihaela;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

public class ProfilActivity extends AppCompatActivity {
    private  String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profil);

        Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.meniu);
        ImageButton omuletBtn=findViewById(R.id.omuletImgBtn);
        omuletBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ProfilActivity.class);
                startActivity(intent);
            }
        });
        ImageButton offBtn=findViewById(R.id.offImgBtn);
        offBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),FeedBackActivity.class);
                startActivity(intent);
            }
        });
        ImageButton setariBtn=findViewById(R.id.setariImgBtn);
        setariBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),GestionareTesteActivity.class);
                startActivity(intent);
            }
        });


        //pentru a cauta in BD exact utilizatorul al carui profil este afisat
        //primim username-ul din activitatea de LOGIN
        this.username = getIntent().getStringExtra("username");

        AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
        adapter.deschideConexiuneBD();
        Utilizator userInitial = adapter.getUserByUsername(this.username);

        if(userInitial!=null) {
            EditText numePrenumeET = findViewById(R.id.numePrenume_ProfulStud);
            EditText emailET = findViewById(R.id.email_ProfilStud);
            EditText facultateET = findViewById(R.id.facultate_ProfilStud);

            numePrenumeET.setText(userInitial.getNumePrenume());
            emailET.setText(userInitial.getEmail());
            facultateET.setText(userInitial.getFacultate());
        }

        //datele studentului logat
        Student stud = adapter.getDateStudent(this.username);
        if(stud!=null) {
            EditText cnpET = findViewById(R.id.CNP_ProfilStud);
            EditText grupaET = findViewById(R.id.grupa_ProfilStud);
            cnpET.setText(stud.getCnp());
            grupaET.setText(stud.getGrupa());
        }

        adapter.inchideConexiuneBD();

    }

    public void editeazaProfilStud(View view) {
        //update pe tabela User si Student DACA ESTE CAZUL!!!

        //date pentru tabela UTILIZATOR
        EditText numePrenumeET = findViewById(R.id.numePrenume_ProfulStud);
        EditText emailET = findViewById(R.id.email_ProfilStud);
        EditText facultateET = findViewById(R.id.facultate_ProfilStud);

        //date pentru tabela STUDENTI
        RadioButton seriaC_RB = findViewById(R.id.seriaCrdBtn);
        RadioButton seriaD_RB = findViewById(R.id.seriaDrdBtn);
        EditText cnpET = findViewById(R.id.CNP_ProfilStud);
        EditText grupaET = findViewById(R.id.grupa_ProfilStud);

        //deschidem conexiunea cu BD
        AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
        adapter.deschideConexiuneBD();

        //ne aducem datele din BD - aferente utilizatoruli actual
         Utilizator user = adapter.getUserByUsername(this.username);

        //verificam daca s-au efectuat modificari pentru tabela UTILIZATOR
        //daca datele introduse de utilizator sunt diferite de cele din tabela (si diferite de null!!!) -> le modificam
        if(!numePrenumeET.getText().toString().isEmpty() && !numePrenumeET.getText().toString().equals(user.getNumePrenume()) ||
                !emailET.getText().toString().isEmpty() && !emailET.getText().toString().equals(user.getEmail()) ||
                !facultateET.getText().toString().isEmpty() && !facultateET.getText().toString().equals(user.getFacultate()))
        {
            //creez un nou utilizator care are atat datele modificate cat si datele vechi care nu se pot modifica
            Utilizator utilizatorModificat = new Utilizator(numePrenumeET.getText().toString(),emailET.getText().toString(),
                    user.getUserName(),user.getParola(),facultateET.getText().toString(),user.getTipCont(),user.getIdSituatie());

            adapter.updateUtilizatori(utilizatorModificat);
        }

        //ne aducem datele din BD - aferente studentului actual
        Student stud = adapter.getDateStudent(this.username);

        //verificam daca s-au efectuat modificari pentru tabela STUDENTI
        //daca datele introduse de utilizator sunt diferite de cele din tabela (si diferite de null!!!) -> le modificam

        String seria;
        if(seriaC_RB.isChecked())
            seria = "C";
             else if(seriaD_RB.isChecked())
                 seria = "D";
             else
                 seria = "E";

         if(!cnpET.getText().toString().isEmpty() && !cnpET.getText().toString().equals(stud.getCnp()) ||
                 !grupaET.getText().toString().isEmpty() && !grupaET.getText().toString().equals(stud.getGrupa()) ||
                 !seria.equals(stud.getSeria()))
         {
            Student studentModificat = new Student(this.username,cnpET.getText().toString(),grupaET.getText().toString(),seria);
            adapter.updateStudenti(studentModificat);
         }
        Toast.makeText(getApplicationContext(),"Datele au fost modificate cu succes!",Toast.LENGTH_LONG).show();
        adapter.inchideConexiuneBD();

    }

    public void stergeProfilStud(View view) {
        //daca tot a fost sters contul -> trimitem utilizatorul in activitatea de feedback
         AdapterBazaDate adapter = new AdapterBazaDate(getApplicationContext(),1);
         adapter.deschideConexiuneBD();
         adapter.deleteUtilizatori(this.username);
         adapter.inchideConexiuneBD();
         Intent intent=new Intent(getApplicationContext(),FeedBackActivity.class);
         startActivity(intent);
    }


    public void onClickRB(View view) {

    }

    //posibilitatea de a ajunge in activitatea pentru teste
    public void testeProfilStud(View view) {
        Intent intent=new Intent(getApplicationContext(),VizualizareTesteActivity.class);
        startActivity(intent);
    }
}
