package com.example.mihaela.proiectmihaela;

import android.os.Parcel;
import android.os.Parcelable;

public class SituatieUtilizator implements Parcelable {

    private int idSituatie;
    private int idTest;
    private int scorTotalU;

    public SituatieUtilizator(int idSituatie, int idTest, int scorTotalU) {
        this.idSituatie = idSituatie;
        this.idTest = idTest;
        this.scorTotalU = scorTotalU;
    }

    protected SituatieUtilizator(Parcel in) {
        idSituatie = in.readInt();
        idTest = in.readInt();
        scorTotalU = in.readInt();
    }

    public static final Creator<SituatieUtilizator> CREATOR = new Creator<SituatieUtilizator>() {
        @Override
        public SituatieUtilizator createFromParcel(Parcel in) {
            return new SituatieUtilizator(in);
        }

        @Override
        public SituatieUtilizator[] newArray(int size) {
            return new SituatieUtilizator[size];
        }
    };

    public int getIdSituatie() {
        return idSituatie;
    }

    public void setIdSituatie(int idSituatie) {
        this.idSituatie = idSituatie;
    }

    public int getIdTest() {
        return idTest;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public int getScorTotalU() {
        return scorTotalU;
    }

    public void setScorTotalU(int scorTotalU) {
        this.scorTotalU = scorTotalU;
    }

    @Override
    public String toString() {
        return "SituatieUtilizator{" +
                "idSituatie=" + idSituatie +
                ", idTest=" + idTest +
                ", scorTotalU=" + scorTotalU +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idSituatie);
        dest.writeInt(idTest);
        dest.writeInt(scorTotalU);
    }
}
