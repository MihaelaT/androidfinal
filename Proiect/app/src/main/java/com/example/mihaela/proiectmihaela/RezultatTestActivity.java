package com.example.mihaela.proiectmihaela;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toolbar;

public class RezultatTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rezultat_test);
        Toolbar toolbar=findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.meniu);

        ImageButton offBtn=findViewById(R.id.offImgBtn);
        offBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),FeedBackActivity.class);
                startActivity(intent);
            }
        });
        ImageButton setariBtn=findViewById(R.id.setariImgBtn);
        setariBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),GestionareTesteActivity.class);
                startActivity(intent);
            }
        });

        //primim notele!
        float nota = getIntent().getFloatExtra("nota",0);
        int nrGresite = getIntent().getIntExtra("nrGresite",0);

        TextView notaTV = findViewById(R.id.notaTxtV);
        notaTV.setText(""+nota);
        RatingBar rb_nota = findViewById(R.id.ratingBarTest);
        rb_nota.setRating(nota);
        TextView intrGresiteTV = findViewById(R.id.nrIntrGTxt);
        intrGresiteTV.setText(""+nrGresite);
    }
}
