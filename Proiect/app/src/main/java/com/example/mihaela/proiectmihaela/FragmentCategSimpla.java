package com.example.mihaela.proiectmihaela;


import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class FragmentCategSimpla extends Fragment {

    public FragmentCategSimpla() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_categ_simpla, container, false);
        Bundle bundle = this.getArguments();

        String detalii = bundle.getString("detalii");
        int an = bundle.getInt("an");
        String nume = bundle.getString("nume");
        final ArrayList<Test> teste = new ArrayList<>();
        int nrIntrebari = bundle.getInt("nrIntreb");
        int categ = bundle.getInt("categorie");
        String numeTest = bundle.getString("numeTest");
        teste.add(new Test(categ,nrIntrebari,null,10,numeTest));

        TextView detaliiTV = view.findViewById(R.id.detalii_frSimplu_TV);
        detaliiTV.setText(detalii);
        TextView anTV = view.findViewById(R.id.anAparitie_fragTV);
        anTV.setText(an+"");
        TextView numeTV = view.findViewById(R.id.numeDezvoltator_fragTV);
        numeTV.setText(nume);
        ListView lv_teste = view.findViewById(R.id.listaTesteFrag_categS);
        TesteAdapter adaptor = new TesteAdapter(view.getContext(),R.layout.layout_teste_adaptor,teste);
        lv_teste.setAdapter(adaptor);
        lv_teste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // obtinem testul selectat
                Test t = (Test) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(view.getContext(),InscriereTestActivity.class);
                intent.putExtra("codTest",t.getIdTest());
                startActivity(intent);
            }
        });

        return view;
    }

}
